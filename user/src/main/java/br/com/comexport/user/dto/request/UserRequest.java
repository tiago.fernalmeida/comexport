package br.com.comexport.user.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Data
public class UserRequest {

    private Long id;

    @NotEmpty(message = "O nome é obrigatório.")
    private String name;

    @NotEmpty(message = "O email é obrigatório.")
    @Email(message = "Informe um email válido.")
    private String email;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "America/Sao_Paulo")
    @NotNull(message = "A data de nascimento é obrigatória")
    private LocalDateTime birthdate;

    private Boolean enabled;

}

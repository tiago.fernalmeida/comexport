package br.com.comexport.user.controller;

import br.com.comexport.user.dto.request.UserRequest;
import br.com.comexport.user.service.UserService;
import br.com.comexport.core.model.User;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@RequestMapping("v1/user")
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private final UserService userService;

    @GetMapping(value = "/consultar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<UserRequest>> consultar(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "email", required = false) String email,
                                                           @RequestParam(value = "birthdate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDateTime birthdate,
                                                           Pageable pageable) {

        UserRequest userRequest = new UserRequest();
        userRequest.setName(name);
        userRequest.setEmail(email);
        userRequest.setBirthdate(birthdate);

        userService.consultar(userRequest);

        //return new ResponseEntity<>(userService.list(pageable), HttpStatus.OK);
        return null;
    }

    //@ApiResponse(responseCode = "201", description = "Usuario criado", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = OrderDto.class))})
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/cadastrar", consumes = "application/json")
    public ResponseEntity<UserRequest> cadastrar(@Valid @RequestBody UserRequest userRequest) {

        userService.cadastrar(userRequest);

        return ResponseEntity.status(HttpStatus.CREATED).body(userRequest);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/alterar", consumes = "application/json")
    public ResponseEntity<UserRequest> alterar(@Valid @RequestBody UserRequest userRequest) {

        userService.alterar(userRequest);

        return ResponseEntity.status(HttpStatus.CREATED).body(userRequest);
    }

}

package br.com.comexport.user.service;

import br.com.comexport.core.repository.UserRepository;
import br.com.comexport.core.model.User;

import br.com.comexport.core.repository.impl.UserRepositoryImpl;
import br.com.comexport.core.specification.UserSearchSpecification;
import br.com.comexport.user.dto.request.UserRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.stream.Collectors;


@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

    private final UserRepository userRepository;

    private final UserRepositoryImpl userRepositoryImpl;

    private static final ModelMapper modelMapper = new ModelMapper();

    public Iterable<User> list(Pageable pageable) {

        return userRepository.findAll(pageable);
    }

    public Page<User> consultar(UserRequest userRequest) {
        User user = modelMapper.map(userRequest, User.class);

        return userRepositoryImpl.consultar(user);
    }

    public Page<UserRequest> busca(UserRequest searchRequest, Pageable pageable){
        User user = modelMapper.map(searchRequest, User.class);
        Page<User> users = userRepository.findAll(new UserSearchSpecification(user), pageable);

        if(users == null) {return new PageImpl<>(new ArrayList<>(), pageable, 0);}

        return new PageImpl<>(users.stream().map(response ->
                new ModelMapper().map(response, UserRequest.class)
        ).collect(Collectors.toList()), pageable, users.getTotalElements());
    }

    public void cadastrar(UserRequest userRequest) {

        User user = modelMapper.map(userRequest, User.class);

        user.setCreatedAt(LocalDateTime.now());
        user.setUpdateAt(LocalDateTime.now());
        user.setEnabled(true);

        userRepository.save(user);

    }

    public void alterar(UserRequest userRequest) {

        User user = modelMapper.map(userRequest, User.class);

        user.setUpdateAt(LocalDateTime.now());

        userRepository.save(user);
    }
}

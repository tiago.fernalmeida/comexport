package br.com.comexport.core.repository;


import br.com.comexport.core.model.User;
import br.com.comexport.core.specification.UserSearchSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    Page<User> findAll(UserSearchSpecification userSearchSpecification, Pageable pageable);
}

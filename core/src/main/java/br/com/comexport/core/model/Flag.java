package br.com.comexport.core.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Flag extends BaseEntity implements AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull(message = "A descrição é obrigatório")
    @Column(nullable = false)
    private String description;

    @NotNull(message = "O status é obrigatório")
    @Column(nullable = false)
    private String enabled;

}

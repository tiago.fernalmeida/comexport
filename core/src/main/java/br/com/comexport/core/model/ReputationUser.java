package br.com.comexport.core.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ReputationUser extends BaseEntity {

    //    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "Id_User")
    @Id
    private Long idUser;

    private Long score;

}

package br.com.comexport.core.repository.impl;

import br.com.comexport.core.model.User;
import br.com.comexport.core.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class UserRepositoryImpl {

    @PersistenceContext
    private EntityManager entityManager;

    public Page<User> consultar(User user) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        cq.select(root);

        if (StringUtils.isNotBlank(user.getName())) {
            cb.and(cb.like(root.get("name"), user.getName()));
        } else if (StringUtils.isNotBlank(user.getEmail())) {
            cb.and(cb.equal(root.get("email"), user.getEmail()));
        } else if (user.getBirthdate() != null) {
            cb.and(cb.equal(root.get("birthdate"), user.getBirthdate()));
        }

        return null;
    }

}

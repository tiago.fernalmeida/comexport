package br.com.comexport.core.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
public class BaseEntity {

    @Column(nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @NotNull(message = "A atualizacao é obrigatória")
    @Column(nullable = false)
    private LocalDateTime updateAt;
}

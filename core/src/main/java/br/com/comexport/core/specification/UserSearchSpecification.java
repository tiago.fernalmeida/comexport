package br.com.comexport.core.specification;

import br.com.comexport.core.model.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class UserSearchSpecification implements Specification<User> {

    private static final long serialVersionUID = 4119929645675846510L;

    private User filters;

    public UserSearchSpecification(User filters) {
        this.filters = filters;
    }

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (filters.getName() != null) {
            predicates.add(criteriaBuilder.like(root.get("name"), filters.getName()));
        }

        if (filters.getEmail() != null) {
            predicates.add(criteriaBuilder.equal(root.get("email"), filters.getEmail()));
        }

        if (filters.getBirthdate() != null) {
            predicates.add(criteriaBuilder.equal(root.get("birthdate"), filters.getBirthdate()));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[] {}));
    }
}
